var ga_ids_regex = new RegExp( "UA-767595-48|UA-1915907-6" );
var domains_regex = new RegExp( "rr1\.com|hollywoodlife\.com" );

var icon_found = 'icon-green.png';
var icon_not_found = 'icon-red.png';
var icon_unknown = 'icon-working.png';

var title_found = 'GA Pageview Found';
var title_not_found = 'GA Pageview NOT Found';
var title_unknown = 'PMC GA Pageview Checker';

var notification_options_found = {
	type: 'basic',
	iconUrl: icon_found, 
	title: 'Pass',
	message: "Pageview found."
};

var notification_options_not_found = {
	type: 'basic',
	iconUrl: icon_not_found,
	title: 'Fail',
	message: "Pageview not found."
};

var listener_filter = { urls: ['<all_urls>'] };

var listen_for_ga = false;
var found_ga = false;
var mutex_lock = false;

var ga_checker_debug = true;

debug_log( '=====Entering GA Checker Code=====' );

/*
	1. On page load, set listen_for_ga to true if URL is in the list
	2. On URL completed with GA, set found_ga to true
	3. After 5 seconds, show positive or negative if GA has fired. Clear status.
*/

chrome.webRequest.onCompleted.addListener(
	function( details ) {
		if( listen_for_ga == true ) {
			debug_log( 'ping' );
			console.log( ga_ids_regex );
			if( details.url.match( ga_ids_regex ) ) {
				debug_log( details.url );
				if( details.url.indexOf( 't=pageview' ) != -1 ) {
					debug_log( 'pong' );
					found_ga = true;
				}
			}
		}

		if( ! mutex_lock ) {
			if( details.url.match( domains_regex ) ) {
				mutex_lock = true;
				listen_for_ga = true;
				setTimeout( show_ga_result, 3000 );
				debug_log( 'turned on ga listening' );
			} 
		}
	},
	listener_filter
);

function show_ga_result( ) {
	if( found_ga ) {
		//chrome.notifications.create( 'ga_checker_notification', notification_options_found );
		response_icon = icon_found;
		response_title = title_found;
	} else {
		chrome.notifications.create( 'ga_checker_notification', notification_options_not_found );
		response_icon = icon_not_found;
		response_title = title_not_found;
	}

	chrome.browserAction.setIcon( { path: response_icon } );
	chrome.browserAction.setTitle( { title: response_title } );

	// Block out all the rest of the results, this means infinite scroll will not register
	listen_for_ga = false;
}

/* 
	Reset the icon on tab change, we don't maintain state 
*/
chrome.tabs.onActivated.addListener( function( activeInfo ) {
	chrome.browserAction.setIcon( { path: icon_unknown } );
	chrome.browserAction.setTitle( { title: title_unknown } );
	mutex_lock = false;
	found_ga = false;
	listen_for_ga = false;
});

chrome.tabs.onUpdated.addListener(function( tabId, changeInfo, tab ) {
	if( changeInfo.url ) {
		debug_log( "URL Changed" );
		chrome.browserAction.setIcon( { path: icon_unknown } );
		chrome.browserAction.setTitle( { title: title_unknown } );
		mutex_lock = false;
		found_ga = false;
		listen_for_ga = false;
	}
});


function debug_log( v ) {
	if( ga_checker_debug ) {
		console.log( v );
	}
}

debug_log( '=====Ending GA Checker Code=====' );